#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
preof - Process Rotary Encoder Output Files.
see LICENSE.txt for details.

Input:
    File(s) containing poll interval, encoder position, other metadata 
    in a sequence, separated by newlines.
Output:
    CSV file(s) with input data arranged in columns.
Invoke:
    $ python preof.py -d <directory>
    or,
    $ python preof.py -f <file 1> <file 2>.. <file N>

"""

import argparse
import errno
import csv
import os
import sys

from datetime import datetime


PREOF_VERSION = "0.2"


def parse_args():
    """User input argument parser.

    Returns
    -------
    args : argparse.Namespace
        An object built up from attributes parsed out of the command line.
    """
    parser = argparse.ArgumentParser(
            prog="PREOF",
            description="Process rotary encoder output files")
    parser.add_argument("-f", "--filepath", nargs="+",
            help="the file to process")
    parser.add_argument("-d", "--dirpath", nargs=1,
            help="the direcoty containing files to process")
    parser.add_argument("--version", action="version", 
            version="%(prog)s {0}".format(PREOF_VERSION))
    return parser.parse_args()


def makedir(dirpath):
    """Creates a directory if it does not exist.

    Parameters
    ----------
    dirpath : str
        Absolute path of the directory to create.

    Raises:
    -------
    OSERROR
        If the exception is not for the entry existing.
    """
    # See: https://stackoverflow.com/a/5032238
    try:
        os.mkdir(dirpath)
        print("created directory: {}".format(dirpath))
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def make_output_paths(filepath):
    """Returns the absolute path to the results directory and the CSV file.
    
    Creates the results directory if it does not exist.

    Parameters
    ----------
    filepath : str
        Absolute path to a file.

    Returns
    -------
    resultsdir : str
        Absolute path to the results directory.
    resultfile : str
        Absolute path to the result file.
    """
    dt = datetime.now()
    dirpath, filename = os.path.split(filepath)
    resultsdir = os.path.join(dirpath, "preof_results", dt.strftime("%y%m%d"))
    filename = ''.join([
        os.path.splitext(filename)[0],
        "-",
        dt.strftime("%y%m%d_%H%M%S"),
        '.csv'])
    resultfile = os.path.join(resultsdir, filename)
    makedir(resultsdir)
    
    return (resultsdir, resultfile)


def gen_result_rows(lines):
    """Generates a sequence of row values.

    Parameters
    ----------
    lines : list
        The Arduino data stream.

    Yields
    ------
    list
        Row values for the CSV result file.
    """
    start_interval = int(lines[0])

    prev_pos = 0
    pos_trial = 0
    curr_tag = "1"
    curr_lap = 1

    i = 0
    j = len(lines) - 2

    while i < j:
        interval, value = lines[i], lines[i + 1]
        
        time_trial = (int(interval) - start_interval) / 1000.
        
        pos, event, pos_cm, speed = None, None, None, None

        if value.startswith("r"):
            pos = value[1:]

            curr_pos = int(pos)
            pos_delta = curr_pos - prev_pos
            prev_pos = curr_pos

            pos_trial = "{0:.5f}".format(curr_pos / 63.) # position in cm
            speed = "{0:.5f}".format(pos_delta / 6.3)
        elif "Tag" in value:
            curr_tag = value[3:].strip()
            if curr_tag == "1":
                curr_lap += 1
        else:
            event = value
        
        yield [curr_lap, interval, pos, curr_tag, event, 
                time_trial, pos_trial, speed]

        i += 2


def process_text_file(filepath):
    """Processes the file containing the Arduino application's output stream.

    Reads the file, calls the method to generate rows, and writes to a CSV file.

    Parameters
    ----------
    filepath : str
        Absolute path to the file to process.
    """
    print("processing: {}".format(filepath))

    # get a directory for the results and get the CSV filepath
    outdir, outfp = make_output_paths(filepath)

    # read lines in the file
    lines = []
    with open(filepath) as f:
        for line in f:
            lines.append(line.strip())
    lines.pop(0) # remove the header - "Imaging started"

    # write results to a CSV file
    with open(outfp, "wt") as f:
        writer = csv.writer(f)
        writer.writerow(["FROM:", filepath])
        writer.writerow([
            "LAP", "INTERVAL", "POSITION", "TAG", "EVENT", 
            "TIME TRIAL (ms)", "POSITION (cm)", "SPEED (cm/s)"])

        # we could have passed the generator below to writerows() but
        # we would like to reset certain values in the first row.
        result_rows = gen_result_rows(lines)

        first_row = next(result_rows)
        for i in (5, 7):
            first_row[i] = 0
        writer.writerow(first_row)

        writer.writerows(result_rows)


def process_file_entities(filepaths):
    """Process a sequence of files entities.

    Files are filtered by their extension - only .txt files are processed.

    Parameters
    ----------
    filepaths : list of str
        A sequence of files to process, each being an absolute path.
    """
    txtfiles = []
    for fp in set(filepaths):
        if fp.endswith(".txt") and os.path.isfile(fp):
            txtfiles.append(fp)
        else:
            print("not processing: {}".format(fp))

    for fp in txtfiles:
        process_text_file(fp)


def process_dir(dirpath):
    """Process all file entries in a directory.

    Parameters
    ----------
    dirpath : str
        An absolute path to the directory.
    """
    print("processing directory: ".format(dirpath))
    entries = []
    for entry in os.listdir(dirpath):
        entries.append(os.path.join(dirpath, entry))
    process_file_entities(entries)


def main():
    args = parse_args()

    # If a directory path is given, process files ending contained within, and 
    # skip any files passed in as arguments.
    if args.dirpath:
        d = args.dirpath[0]
        if os.path.isdir(d):
            process_dir(os.path.abspath(d))
    elif args.filepath:
        process_file_entities(args.filepath)

if __name__ == '__main__':
    sys.exit(main())

